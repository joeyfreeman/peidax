﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Functionality  of the graphical part of handling a bar. 
 * We can attach this to any type of character and use it for any type of bar (Health, Energy, etc.)
 */
 [System.Serializable]
public class UIBar : MonoBehaviour
{
    protected float m_fillAmount;

    protected float m_CurrentValue;
    protected float m_MaximumValue;

    [SerializeField]
    protected Image m_barContent;


    protected Color m_barColor;

    /*          ------public void UpdateBar-------
    *
    * Gets the current and maximum values from whatever object is using its bar
    * Handle any changes we need to do to a bar such as change
    * the fill amount, test, color, etc..
    * 
    */
    public void UpdateBar(float currVal, float maxVal)
    {
        // Update how much we want to fill the bar
        m_CurrentValue = currVal;
        m_MaximumValue = maxVal;

        m_barContent.fillAmount = UpdateFillAmount(m_CurrentValue, m_MaximumValue);
    }

    /*          -----public float UpdateFillAmount-----
     * Get a fill value between 0 and 1, so we know how much to fill the bar
     */
    public float UpdateFillAmount(float currVal, float maxVal)
    {

        m_CurrentValue = currVal;
        m_MaximumValue = maxVal;

        m_fillAmount = m_CurrentValue / m_MaximumValue;
      
        // Make sure fill amount doesn't go below 0 or above 1
        if (m_fillAmount <= 0)
        {
            m_fillAmount = 0;
        }

        if (m_fillAmount >= 1)
        {
            m_fillAmount = 1;
        }

        return m_fillAmount;
    }

    

}
