﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShieldBar : UIBar
{
    public IEnumerator TickCooldown(float cooldown)
    {
        m_CurrentValue = cooldown;
        m_MaximumValue = m_CurrentValue;

        while (m_CurrentValue > 0)
        {
            UpdateBar(m_CurrentValue, m_MaximumValue);

            yield return new WaitForSeconds(1.0f);

            m_CurrentValue--;

            if (m_CurrentValue <= 0)
            {
                UpdateBar(0, 0);
            }
        }
    }
}
