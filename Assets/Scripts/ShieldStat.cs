﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldStat : Stat
{ 
    public void InitializeShield(float currentShield, float maxShield)
    {
        Initialize(currentShield, maxShield);
    }

    public float GetCurrentShield()
    {
        return CurrentValue;
    }

    public float GetMaxShield()
    {
        return MaxValue;
    }

    public void SetShield(float shieldAmount)
    {
        SetValue(shieldAmount);
    }

    public void DamageShield(float damageAmount)
    {
        DecreaseValue(damageAmount);
    }

    public void RestoreShield(float shieldAmount)
    {
        IncreaseValue(shieldAmount);
    }
}
