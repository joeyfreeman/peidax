﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/*
 * The BaseCharacter class will hold anything that is common between any types of character (Player, Enemie, NPC..), 
 * it will most likely be used mostly to hold stat values for health, damage, etc..
 * 
 * Every character object we create, will derive from this class.
 * 
 * Character Controls like movement, attacks, animations, will take place in the XCharacterObjectController scripts
 * 
 */
 [System.Serializable]
public abstract class BaseCharacter : MonoBehaviour
{
    protected HealthStat m_health;

    public BaseCharacter()
    {
        m_health = new HealthStat();
    }

    public bool IsDead()
    {
        if (m_health.CurrentValue > 0)
        {
            return false;
        }

        return true;
    }

    public HealthStat GetHealthStat()
    {
        return m_health;
    }

    public abstract IEnumerator Die();
}
