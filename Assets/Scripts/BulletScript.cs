﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

	const int MAX_DISTANCE = 15; //max distance a bullet can travel before destroying itself
	Vector3 startPos; 
	float bulletSpeed = 20f;
	const float bulletSpread = 2f;
	public string direction = "right";
	Rigidbody rb;

	// Use this for initialization 
	void Start () {
		startPos = transform.position;
		if (direction == "left") {
			bulletSpeed = -bulletSpeed;
		}
		rb = GetComponent<Rigidbody> ();
		rb.velocity = new Vector3 (bulletSpeed, Random.Range(-bulletSpread, bulletSpread), Random.Range(-bulletSpread, bulletSpread));
		
	}

	// Update is called once per frame
	void Update () {
		if (Vector3.Distance (transform.position, startPos) > MAX_DISTANCE) { //if too far from start position, destroy self
			Destroy (gameObject);
		}
	}

    public void OnCollisionEnter(Collision collision)
    {
		if (collision.collider.tag != "Shield") {
			Destroy (gameObject);
		} 

		if (collision.collider.tag == "Player") {
            PlayerController player = collision.collider.gameObject.GetComponent<PlayerController>();

            HealthStat playerHealth = player.GetHealthStat();
            playerHealth.TakeDamage(5);
        } 

		if (collision.collider.tag == "Enemy") {
			
			GuardController enemy = collision.collider.gameObject.GetComponent<GuardController>();

			HealthStat enemyHealth = enemy.GetHealthStat();

			enemyHealth.TakeDamage(40);
		}
    }
}
