﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HealthStat : Stat
{ 
    public void InitializeHealth(float currentHealth, float maxHealth)
    {
        Initialize(currentHealth, maxHealth);
    }

    public float GetCurrentHealth()
    {
        return CurrentValue;
    }

    public float GetMaxHealth()
    {
        return MaxValue;
    }

    public void SetHealth(float healthAmount)
    {
        SetValue(healthAmount);
    }

    public void TakeDamage(float damageAmount)
    {
        DecreaseValue(damageAmount);
    }

    public void RestoreHealth(float healAmount)
    {
        IncreaseValue(healAmount);
    }
}
