﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * The Controller for the Player. This will handle all of Peidax's controls like movement,
 * attacking, death, and animations. Inherits from the BaseCharacter.
 * 
 */
 [System.Serializable]
public class PlayerController : BaseCharacter
{
    [SerializeField]
    private float m_speed;
    
    private Rigidbody m_rb;
	private float idleTime;

    private HealthBar m_healthBar;

    // Position where abilities are launched from
    private Vector3 m_firePoint;

	public string m_direction;

    // holds prefab for energy ball ability functionality
    [SerializeField]
    private GameObject m_EnergyBall;
    private float m_energyBallCooldownDuration;
    private float m_energyBallNextReadyTime;

    [SerializeField]
    private GameObject m_ShieldAbilityOrb;
    private float m_shieldCooldownDuration;
    private float m_nextShieldReadyTime;

    ShieldBar m_shieldAbilityBar;

    [SerializeField]
    GameObject m_stunRay;
    private float m_stunRayCooldownDuration;
    private float m_nextStunRayReadyTime;
    StunRayBar m_stunRayBar;

    Animator m_anim;

	bool performingAbility = false;
	float abilityTimer = 0.0f;
	float abilityWaitTime = 1.0f;

	bool onFloor = true;

    private ShieldStat m_shield;

	Text winText;

    [SerializeField] private Transform m_shieldStartPos;

    void Start()
    {
        winText = GameObject.Find("WinText").GetComponent<Text>();
        winText.enabled = false;

        m_rb = GetComponent<Rigidbody>();
        m_healthBar = GetComponent<HealthBar>();
        m_shieldAbilityBar = GetComponent<ShieldBar>();

        m_firePoint = getFirePoint();//instantiate fire point to player transform
        m_direction = "right";

        m_speed = 7.5f;

        m_health.InitializeHealth(100f, 100f);

        //Gets animator components
        m_anim = GetComponent<Animator>();


        m_shield = new ShieldStat();

        m_shield.InitializeShield(0f, 25f);

        m_energyBallCooldownDuration = 1f;
        m_shieldCooldownDuration = 10f;
        m_stunRayCooldownDuration = 7f;

        m_stunRayBar = GetComponent<StunRayBar>();
    }

	//Return fire point Vector3 based on player position
	public Vector3 getFirePoint() {
		int x_offset = 2;
		if (m_direction == "left") {
			x_offset = -x_offset;
		}

		return new Vector3 (transform.position.x + x_offset, transform.position.y + 1, transform.position.z);
	}

    void Update()
    {
        if (IsDead())
        {
            StartCoroutine(Die());
        }

		if (Input.GetKey ("l")) { //reload scene when L pressed
			SceneManager.LoadScene ("scene1");
		}

        MovePlayer();
        HandleAbilities();
        m_healthBar.UpdateBar(m_health.GetCurrentHealth(), m_health.GetMaxHealth());

		if (performingAbility) {
			abilityTimer += Time.deltaTime;
			if (abilityTimer > abilityWaitTime) {
				performingAbility = false;
				abilityTimer = 0.0f;
			}
    	}
	}

    public void HandleAbilities()
    {
		m_firePoint = getFirePoint(); //update fire point

		if (Input.GetKeyDown(KeyCode.F) && !IsDead() && onFloor && !performingAbility && (Time.time > m_energyBallNextReadyTime))
        {
            m_energyBallNextReadyTime = Time.time + m_energyBallCooldownDuration;
			m_anim.SetTrigger ("Fireball");

			performingAbility = true;
			abilityTimer = 0;
        }

        if (Input.GetKeyDown(KeyCode.R) && !IsDead() && (Time.time > m_nextShieldReadyTime))
        {
            performingAbility = true;
            abilityTimer = 0;

            m_nextShieldReadyTime = Time.time + m_shieldCooldownDuration;

            Instantiate(m_ShieldAbilityOrb, m_shieldStartPos.position, Quaternion.Euler(0f, 90f, 0f));

            StartCoroutine(m_shieldAbilityBar.TickCooldown(m_shieldCooldownDuration));
        }

        if (Input.GetKeyDown(KeyCode.T) && !IsDead() && (Time.time > m_nextStunRayReadyTime))
        {
            Instantiate(m_stunRay, m_firePoint, Quaternion.Euler(0f, 0f, 90f));

            m_nextStunRayReadyTime = Time.time + m_stunRayCooldownDuration;

            StartCoroutine(m_stunRayBar.TickCooldown(m_stunRayCooldownDuration));
        }
    }

	void CastEnergy ()
	{
		Instantiate(m_EnergyBall, m_firePoint, Quaternion.Euler(0f, 180f, 0f));
	}

	//Eventually will add an animating function instead of just adding animations to the movePlayer function

    void MovePlayer()
    {
		if (!IsDead () && onFloor) {

			float xVelocity = 0.0f;
			float yVelocity = 0.0f;

			if (Input.GetKey ("right")) {
				xVelocity = m_speed;
				m_direction = "right";
				transform.eulerAngles = new Vector3 (0, 90, 0);
			} else if (Input.GetKey ("left")) {
				xVelocity= -m_speed;
				m_direction = "left";
				transform.eulerAngles = new Vector3 (0, -90, 0);
			}

			if (Input.GetKey("up") && !performingAbility) { //jumping
				xVelocity *= 1.5f;
				yVelocity = 10.0f;
			} else if ((Input.GetKey ("right") || Input.GetKey("left")) && !Input.GetKey("up")) { //running
				m_anim.SetBool ("IsMoving", true);
				performingAbility = false; //interrupt ability
				abilityTimer = 0.0f;
				idleTime = 0f;
			} else { //standing still
				idleTime += Time.deltaTime;
				m_anim.SetBool ("IsMoving", false);
				m_anim.SetFloat ("IdleTime", idleTime);
			}

			m_rb.velocity = new Vector3 (xVelocity, yVelocity, 0);
		}
    }

    public ShieldStat GetShieldStat()
    {
        return m_shield;
    }

    public Vector3 GetShieldPosition()
    {
        Vector3 shieldPos = new Vector3(m_shieldStartPos.position.x, m_shieldStartPos.position.y,
                                        m_shieldStartPos.position.z);

        return shieldPos;
    }

    public override IEnumerator Die()
    {
		m_rb.velocity = new Vector3 (0, m_rb.velocity.y, 0);

        m_anim.SetBool("IsDead", true);

        yield return new WaitForSeconds(3.5f);

        SceneManager.LoadScene("GameOver");

        yield return new WaitForSeconds(120f);


    }

    public bool IsPerformingAbility()
    {
        return performingAbility;
    }

	public void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.tag == "Floor") {
			m_anim.SetBool ("OnFloor", true);
			onFloor = true;
		}

	}

	public void OnCollisionStay(Collision other)
	{
		if (!onFloor && other.collider.tag == "Enemy") {
			other.collider.GetComponent<GuardController> ().GetHealthStat ().TakeDamage (5);
		}
	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Goal") {
			winText.enabled = true;
			StartCoroutine (Win());
		}
	}

	public IEnumerator Win()
	{
		yield return new WaitForSeconds(3.0f);

		SceneManager.LoadScene ("MainMenu");
	}



	public void OnTriggerStay(Collider other)
	{
		if (other.tag == "Laser") {
			GetHealthStat ().TakeDamage (1);
		}
	}

	public void OnCollisionExit(Collision collision)
	{
		if (collision.collider.tag == "Floor") {
			m_anim.SetBool ("OnFloor", false);
			onFloor = false;
		}
	}
		
}


