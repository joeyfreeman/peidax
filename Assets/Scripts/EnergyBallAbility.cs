﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBallAbility : Ability
{
    private PlayerController m_player;

    private float m_energyBallSpeed;
    private float m_energyBallRange;
    private float m_energyBallDamage;

    Vector3 m_startPos;

    public void Start()
    {

        m_energyBallSpeed = 10f;
        m_energyBallDamage = 25f;
        m_energyBallRange = 15f;

        m_player = FindObjectOfType<PlayerController>(); 

        // If the player is facing left, shoot the ball from the left
        if (m_player.transform.rotation.eulerAngles.y != 90)
        {
            m_energyBallSpeed = -m_energyBallSpeed;
        }

        m_startPos = transform.position;
    }

    void Update()
    {
        TriggerAbility();
    }

    public override void TriggerAbility()
    {
        // Shoot the ball with velocity
        GetComponent<Rigidbody>().velocity
                    = new Vector3(m_energyBallSpeed, 0, 0);

        // If the energy ball goes too far, destroy it
        if (Vector3.Distance(transform.position, m_startPos) > m_energyBallRange) 
        { 
            Destroy(gameObject);
        }
    }
    
    /* If the EnergyBall collides with an enemy, deal damage to the enemy
     * and destroy the EnergyBall
     */
    public void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);

        if (collision.collider.tag == "Enemy")
        {
            /* Will need to change when we add more enemies.
             * Might be easiest to have a BaseEnemy that inherits from BaseCharacter
             * then have every enemy inherit from BaseEnemy instead of BaseCharacter?
            */
            GuardController enemy = collision.collider.gameObject.GetComponent<GuardController>();

            HealthStat enemyHealth = enemy.GetHealthStat();

            enemyHealth.TakeDamage(m_energyBallDamage);
        }
    }
}
