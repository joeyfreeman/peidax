﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/* 
 * Holds a character stat (Health, Energy, Shield)
 */
 [System.Serializable]
public class Stat
{
    private float m_currentValue { get; set; }
    private float m_maxValue { get; set; }

    public float CurrentValue
    {
        get { return m_currentValue; }
        set { m_currentValue = value; }
    }

    public float MaxValue
    {
        get { return m_maxValue; }
        set { m_maxValue = value; }
    }

    public Stat()
    {
        CurrentValue = 10f;
        MaxValue = 10f;
    }

    /*
     * ----- public void InitializeStat(int maxStatValue)
     * 
     * - Give the stat its initial values based on its max value
     * - current value will always start at the max (100%)
     */
    public void Initialize(float currentValue, float maxValue)
    {
        m_currentValue = currentValue;
        m_maxValue = maxValue;
    }

    public void SetValue(float statValue)
    {
        m_currentValue = statValue;
    }

    public void SetMaxValue(float maxValue)
    {
        m_maxValue = maxValue;
    }

    public void DecreaseValue(float decreaseAmount)
    {
        m_currentValue -= decreaseAmount;

        if (m_currentValue < 0)
        {
            m_currentValue = 0;
        }
    }

    public void IncreaseValue(float increaseAmount)
    {
        m_currentValue += increaseAmount;

        if (m_currentValue > m_maxValue)
        {
            m_currentValue = m_maxValue;
        }
    }
}
