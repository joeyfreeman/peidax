﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class HealthBar : UIBar
{
    [SerializeField]
    protected Text m_barText;


    void Start()
    {
        m_barContent.color = Color.green;
    }

    void Update ()
    {
        m_barContent.color = UpdateBarColor();
        UpdateBarText();
	}


    /*
     * -----public Color ChangeBarColor()-----
     * Changes the color of the bar based on the fill amount of the bar
     * > 50% = Green
     * < 50% & > 25% = Yellow
     * < 25% = Red
     */
    public Color UpdateBarColor()
    {
        if (m_fillAmount < .25F)
        {
            m_barColor = Color.red;
        }
        else if(  (m_fillAmount > .25F) && (m_fillAmount <= .50F) )
        {
            m_barColor = Color.yellow;
        }
        else
        {
            m_barColor = Color.green;
        }

        return m_barColor;
        
    }

    /* -----public void UpdateBarText()
     *
     * Updates the text of the bar based on the current value
     */
    public void UpdateBarText()
    {
        m_barText.text = m_CurrentValue.ToString();
    }



}
