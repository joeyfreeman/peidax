﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Controller for the guard enemy. Handles all AI movement, attacking, and animations
 */
public class GuardController : BaseCharacter
{ 
	Animator m_anim;
	PlayerController player;
	const int SIGHT_DISTANCE = 13;
	bool playerClose = false;
	[SerializeField]
	AudioClip singleShotClip;
	[SerializeField]
	GameObject bullet;
	string m_direction = "left";

    private bool m_isStunned;

    public bool StunnedStatus { get; set; }

	Material m_material;
	Renderer m_renderer;

    void Start ()
    {
		m_renderer = GetComponentInChildren<Renderer> ();
		m_material = m_renderer.material;
        m_isStunned = false;

        m_health.InitializeHealth(50f, 50f);

        player = FindObjectOfType<PlayerController>();

		//Gets animator components
		m_anim = GetComponent<Animator>();

	}

	void fireSingle() {
		AudioSource.PlayClipAtPoint (singleShotClip, transform.position);
		GameObject newBullet = Instantiate (bullet, getFirePoint (), Quaternion.Euler(0f, 0f, 0f));
		newBullet.GetComponent<BulletScript>().direction = m_direction;

	}

	Vector3 getFirePoint() {
		float x_offset = 1.7f;
		if (m_direction == "left") {
			x_offset = -x_offset;
		}

		return new Vector3 (transform.position.x + x_offset, transform.position.y + 1.6f, transform.position.z);
	}

	void Update()
	{
        if (IsDead())
        {
            StartCoroutine(Die());
        }

		if (!m_isStunned) {
			if (player.transform.position.x < transform.position.x) { //if player is left of guard, rotate to face left
				m_direction = "left";
				transform.eulerAngles = new Vector3 (0, -90, 0);
			} else { //otherwise face right
				m_direction = "right";
				transform.eulerAngles = new Vector3 (0, 90, 0);
			}
		}


		if (playerClose == false && m_isStunned == false &&!player.IsDead() && Vector3.Distance(transform.position, player.transform.position) < SIGHT_DISTANCE)
		{
			playerClose = true;
			m_anim.SetBool ("playerClose", true);

		} else {
			playerClose = false;
			m_anim.SetBool ("playerClose", false);
		}

	}

    public IEnumerator HandleStunDebuff()
    {
        float stunDuration = 3f;

        while (stunDuration > 0)
        {
            yield return new WaitForSeconds(1f);

            stunDuration--;
        }
			
		m_renderer.material = m_material;
		m_isStunned = false;
    }

    public override IEnumerator Die()
    {
        yield return new WaitForSeconds(0.1f);

        // Play Death Animation

        // Destroy the object
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "AG_RAY")
        {
			m_renderer.material = collision.collider.GetComponent<Renderer> ().material; //change material to green glow
            m_isStunned = true;
			float offset = 2f;
			if (collision.collider.transform.position.x > transform.position.x) { //knock back other way if stun ray is on other side
				offset = -offset;
			}
            transform.position = new Vector3(transform.position.x + offset, transform.position.y, transform.position.z);
            StartCoroutine(HandleStunDebuff());
        }
    }
}
