﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiGravityStunAbility : Ability
{
    private PlayerController m_player;

    private float m_antiGravityRaySpeed;
    private float m_antiGravityRayRange;

    Vector3 m_startPos;

    private void Start()
    {
        m_player = FindObjectOfType<PlayerController>();

        m_antiGravityRaySpeed = 20f;
        m_antiGravityRayRange = 10f;

        if (m_player.transform.rotation.eulerAngles.y != 90)
        {
            m_antiGravityRaySpeed = -m_antiGravityRaySpeed;
        }

        m_startPos = transform.position;
    }

    private void Update()
    {
        TriggerAbility();
    }

    public override void TriggerAbility()
    {
        GetComponent<Rigidbody>().velocity =
            new Vector3(m_antiGravityRaySpeed, 0, 0);

        if (Vector3.Distance(transform.position, m_startPos) > m_antiGravityRayRange) 
        {
            Destroy(gameObject);
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Enemy")
        {
            Destroy(gameObject);
        }
    }
}
