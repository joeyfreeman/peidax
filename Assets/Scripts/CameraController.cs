﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	[SerializeField] private GameObject player;
	private int cameraTranslationSpeed = 30;
	private string cameraDirection = "right";
	int x_offset = 5;

	// Use this for initialization
	void Start () {
		
	}

	void LateUpdate () {
		string playerDirection = player.GetComponent<PlayerController> ().m_direction;
		if (playerDirection == "right") {
			x_offset = Mathf.Abs (x_offset);
		} else {
			x_offset = -Mathf.Abs (x_offset);
		}
		Vector3 new_position = new Vector3 (player.transform.position.x + x_offset, this.transform.position.y, this.transform.position.z);
		if (cameraDirection != playerDirection) { //if player has turned around, gently translate camera to other side
			Vector3 old_position = transform.position;
			if (Vector3.Distance (old_position, new_position) > 1) { //prevent shaking due to imprecise translation
				Vector3 translation = new_position - old_position;
				translation.Normalize ();
				transform.Translate (translation * Time.deltaTime * cameraTranslationSpeed);
			} else {
				cameraDirection = playerDirection;
			}
		} else { //otherwise instantly move camera to new position
			transform.position = new_position;
		}

	}
}
