﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldAbility : Ability
{
    private PlayerController m_player;
    private ShieldStat m_playerShield;

    private float m_shieldAmount;
    private float m_shieldMaxDuration;
    private float m_shieldTimeLeft;

    private bool m_shieldOn;

    void Start()
    {
        m_player = FindObjectOfType<PlayerController>();

        m_shieldAmount = 25f;
        m_shieldMaxDuration = 6f;

        m_shieldOn = false;
     }

    void Update()
    {
        transform.position = m_player.GetShieldPosition();

        // Make sure that the ability doesn't get triggered if it's already
        // active

        if (m_shieldTimeLeft <= 0)
        {
            TriggerAbility();
        }
    }

    public override void TriggerAbility()
    {
        // Get a handle to the player's health and shield
        m_playerShield = m_player.GetShieldStat();

        // Give player the shield buff
        m_playerShield.SetShield(m_shieldAmount);

        m_shieldOn = true;

        StartCoroutine(HandleShieldTimer());
    }

    public IEnumerator HandleShieldTimer()
    {
        m_shieldTimeLeft = m_shieldMaxDuration;

        while (m_shieldTimeLeft > 0)
        {
            yield return new WaitForSeconds(1.0f);

            m_shieldTimeLeft--;
        }

        m_shieldTimeLeft = 0;
        m_shieldOn = false;

        Destroy(gameObject);
    }

    public bool IsShieldOn()
    {
        return m_shieldOn;
    }

    public void OnCollisionEnter(Collision collision)
    {
		//we have to check if the shield is still on in case it has already been deleted by the shield timer thread
        if (m_shieldOn && collision.collider.tag == "Bullet")
        {
            m_shieldAmount -= 2f;
            m_playerShield.DamageShield(2f);

            if (m_shieldAmount <= 0)
            {
                Destroy(gameObject);

                StopCoroutine(HandleShieldTimer());
                m_shieldTimeLeft = 0;
            }
        }
    }
}
